# msea: Microbe-Set Enrichment Analysis (MSEA) for functional interpretation of sets of microbial organisms

## What is it?
**msea** a Python package implementing the Microbe-Set Enrichment Analysis pipeline, helping microbiologists make sense of collections microbes via external annotations.

## Installation
```sh
# from PyPI
pip install msea
```

## Dependencies
- [NumPy](https://www.numpy.org)
- [SciPy](https://www.scipy.org/)
- [pandas](https://pandas.pydata.org/)

## Tutorials and Documentations

Please refer to [examples](examples) or read the [full documentation](https://msea.readthedocs.io/en/latest/). 

## Development

For local development: 
```sh
python setup.py develop
```

To run tests:
```sh
python setup.py test
```

To build documentation with Sphinx:
```sh
cd docs && make html
```

To build distribution: 
```sh
python setup.py bdist bdist_wheel
# then upload to PyPI
python -m twine upload dist/*
```
## License
[BSD 3](LICENSE)
