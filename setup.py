import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="msea",
    version="0.0.2",
    author="Zichen Wang",
    author_email="wangzc921@gmail.com",
    description="Implementation of Microbe-Set Enrichment Analysis (MSEA).",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/wangz10/msea",
    packages=setuptools.find_packages(),
    license='BSD',
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.5',
    install_requires=['numpy', 'pandas', 'scipy', 'tqdm'],
    include_package_data=True,
    test_suite='nose.collector',
    tests_require=['nose'],
)
