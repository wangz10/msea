'''
A case study demonstrating how MSEA can be integrated into a computational
pipeline analyzing a microbiome profiling dataset.
'''

import numpy as np
import pandas as pd
from biom import load_table  # reqired for parsing BIOM formated dataset
# pip install biom-format
from skbio.stats.composition import ancom
# pip install scikit-bio
import msea
from msea import SetLibrary

# 0. Load and prepare 16S dataset
# The 16S dataset was downloaded from
# [Qiita](https://qiita.ucsd.edu/study/description/10483#)
table = load_table('../msea/data/study_10483/otu_table.biom')
print(table.shape)  # OTUs x samples
# Optionally normalize data on the sample axis -> relative abundance
# table.norm(axis='sample', inplace=True)

# Load metadata
meta_df = pd.read_csv(
    '../msea/data/study_10483/10483_prep_2122_qiime_20180418-105538.txt',
    sep='\t',
    index_col='#SampleID')
meta_df = meta_df.loc[table.ids()]


def collapse_f(id_, md):
    # collapse OTUs to genus-level
    genus_idx = 5
    return '; '.join(md['taxonomy'][:genus_idx + 1])


table_g = table.collapse(collapse_f, axis='observation')
print(table_g.shape)

# check the microbial species:
print(table_g.ids(axis='observation')[:5])
# convert to a pd.DataFrame
df_g = pd.DataFrame(
    table_g.matrix_data.toarray().T,
    columns=table_g.ids(axis='observation'),
    index=table_g.ids()
)
# Select a subset of samples from genotype 'Thy1-aSyn' for further analysis
meta_df_sub = meta_df.loc[meta_df['genotype'] == 'Thy1-aSyn']
df_g_sub = df_g.loc[meta_df['genotype'] == 'Thy1-aSyn']
print(meta_df_sub['donor_status'].value_counts())


# 1. Perform DA analysis using ANCOM to identify DA microbes
ancom_df, percentile_df = ancom(df_g_sub + 1,  # adding pseudocounts
                                meta_df_sub['donor_status'],
                                alpha=0.1,
                                multiple_comparisons_correction='holm-bonferroni')
microbes_DA = ancom_df.loc[ancom_df['Reject null hypothesis']].index
print(microbes_DA)

# retain genus names
microbes_DA = filter(None, [s.split('; ')[-1].lstrip('g__')
                            for s in microbes_DA])
# convert to set
microbes_DA = set(microbes_DA)
print(microbes_DA)


# 2. MSEA for DA microbes
# Load reference microbe-set library
set_lib = SetLibrary.load(
    '../msea/data/human_genes_associated_microbes/set_library.gmt',
    rank_means_file='../msea/data/human_genes_associated_microbes/rank_means.npy',
    rank_stds_file='../msea/data/human_genes_associated_microbes/rank_stds.npy')
msea_result = set_lib.enrich(microbes_DA, adjust=True, universe=1000)
print(msea_result.head())
