'''
Walkthrough of basic functionalities in the MSEA package.
'''
from pprint import pprint
import msea
from msea import SetLibrary

# 0. load a reference microbe-set library from a GMT file
gmt_filepath = \
    'https://bitbucket.org/wangz10/msea/raw/aee6dd184e9bde152b4d7c2f3c7245efc1b80d23/msea/data/human_genes_associated_microbes/set_library.gmt'

# msea package provides a `read_gmt` function to parse a GMT file into a
# dictionary of sets
d_gmt = msea.read_gmt(gmt_filepath)
print('Number of microbe-sets:', len(d_gmt))
# Look at a couple of reference sets in the library
pprint(list(d_gmt.items())[:3])


# 1. perform MSEA for a input microbe-set against the library of reference sets
microbe_set_input = set(['Colwellia',
                         'Deinococcus',
                         'Idiomarina',
                         'Neisseria',
                         'Pseudidiomarina',
                         'Pseudoalteromonas'])
# this can be done using the `msea.enrich` function
msea_result = msea.enrich(microbe_set_input, d_gmt=d_gmt, universe=1000)
# check the top enriched reference microbe-sets
print(msea_result.head())


# 2. perform MSEA with adjustment of expected ranks for reference sets
# Sometimes certain reference microbe-sets in a library are more likely to
# be enriched by chance. We can adjust this by empirically estimating the
# null distributions of the ranks of the reference sets, then uses z-score
# to quantify if observed ranks are significantly different from the
# expected ranks.

# To do that, it is easier through the `SetLibrary` class.
set_library = SetLibrary.load(gmt_filepath)
# The `SetLibrary.get_empirical_ranks` method helps compute the expected
# ranks and store the means and stds of the ranks from the null
# distribution
set_library.get_empirical_ranks(n=20)
print(set_library.rank_means.shape, set_library.rank_stds.shape)
# After that, we can perform MSEA with this adjustment
msea_result_adj = set_library.enrich(
    microbe_set_input, adjust=True, universe=1000)
print(msea_result_adj.head())
