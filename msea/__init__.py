'''
"msea" a Python package implementing the Microbe-Set Enrichment Analysis
pipeline, helping microbiologists make sense of collections microbes via
external annotations.
'''
from .utils import *
from .set_library import *
