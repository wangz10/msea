.. _quickstart:

Quick start
===========

Installation
------------
To install from PyPI::

   # from PyPI
   pip install msea


Dependencies
------------
* `NumPy <http://www.numpy.org/>`_
* `SciPy <http://www.scipy.org/>`_
* `pandas <https://pandas.pydata.org/>`_


Development
-----------

For local development::
   
   python setup.py develop


To run tests::

   python setup.py test



To build distribution::

   python setup.py bdist bdist_wheel
   # then upload to PyPI
   python -m twine upload dist/*

To build Sphinx documentation::

    cd docs && make html
    