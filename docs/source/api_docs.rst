.. _api_docs:

API documentations
==================

.. _set_library:

SetLibrary
-----------
The data structure for microbe-set SetLibrary

.. autoclass:: msea.SetLibrary
   :members:

.. _utils:

Utility functions
------------------
.. automodule:: msea.utils
   :members:
