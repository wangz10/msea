.. msea documentation master file, created by
   sphinx-quickstart on Fri May  1 11:14:14 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to msea's documentation!
================================
Microbe-Set Enrichment Analysis (MSEA) for functional interpretation of sets of microbial organisms

What is it?
-----------
**msea** is a Python package implementing the Microbe-Set Enrichment Analysis pipeline, helping microbiologists make sense of collections microbes via external annotations.

Table of Contents
=================
.. toctree::
   :maxdepth: 2
   
   quickstart
   tutorial
   api_docs
   license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
